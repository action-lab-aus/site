# ACTION Lab Website

To update:
- Edit files via GitLab and commit.

For local editing and preview:

- Clone this repository and run `hugo serve`. Visit page at http://localhost:1313.