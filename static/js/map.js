(function() {
    var globe = planetaryjs.planet();
    // Load our custom `autorotate` plugin; see below.
    globe.loadPlugin(autorotate(5));
    // The `earth` plugin draws the oceans and the land; it's actually
    // a combination of several separate built-in plugins.
    //
    // Note that we're loading a special TopoJSON file
    // (world-110m-withlakes.json) so we can render lakes.
    globe.loadPlugin(planetaryjs.plugins.earth({
      topojson: { file:   '/js/world-110m.json' },
      oceans: {fill: 'transparent'},
      land: {fill:'#C24D6733'},
      borders:{stroke: 'white', lineWidth: 5, type: 'both'},
    //   borders:{stroke: 'red', lineWidth: 5, type: 'internal'}
    }));
    // Load our custom `lakes` plugin to draw lakes; see below.
    // globe.loadPlugin(lakes({
    //   fill: '#000080'
    // }));
    // The `pings` plugin draws animated pings on the globe.
    globe.loadPlugin(planetaryjs.plugins.pings());
    // The `zoom` and `drag` plugins enable
    // manipulating the globe with the mouse.
    // globe.loadPlugin(planetaryjs.plugins.zoom({
    //   scaleExtent: [100, 300]
    // }));
    // globe.loadPlugin(planetaryjs.plugins.drag({
    //   // Dragging the globe should pause the
    //   // automatic rotation until we release the mouse.
    //   onDragStart: function() {
    //     this.plugins.autorotate.pause();
    //   },
    //   onDragEnd: function() {
    //     this.plugins.autorotate.resume();
    //   }
    // }));
    // Set up the globe's initial scale, offset, and rotation.
    globe.projection.scale(1000).translate([1000, 500]).rotate([180, 10, 0]);
  
    // Every few hundred milliseconds, we'll draw another random ping.
    // var colors = ['pink'];
    var places = [
        [54.9771,-1.61421],
        [-19.5644204,18.1040894],
        [-1.2398711,116.8593379],
        [26.2540493,29.2675469],
        [24.4768783,90.2932426],
        [9.6000359,7.9999721],
        [22.3511148,78.6677428],
        [-37.8762306,145.0437106],
        [-33.88890695,151.1894337],
        [2.8894434,-73.783892],
        [15.2572432,-86.0755145]
        
    ];

    function dopoints()
    {
        var place;
        for(i in places)
        {
            // var lat = Math.random() * 170 - 85;
            // var lng = Math.random() * 360 - 180;
            // console.log(places[i]);
            globe.plugins.pings.add(places[i][1], places[i][0], { color: '#C24D67', ttl: 5000, angle: 5});
        }
    }


    setInterval(dopoints, 2000);
    
  
    var canvas = document.getElementById('rotatingGlobe');
    // Special code to handle high-density displays (e.g. retina, some phones)
    // In the future, Planetary.js will handle this by itself (or via a plugin).
    // if (window.devicePixelRatio == 2) {
    //   canvas.width = 2000;
    //   canvas.height = 2000;
    //   context = canvas.getContext('2d');
    //   context.scale(2, 2);
    // }
    // Draw that globe!
    globe.draw(canvas);
    dopoints();


    // This plugin will automatically rotate the globe around its vertical
  // axis a configured number of degrees every second.
  function autorotate(degPerSec) {
    // Planetary.js plugins are functions that take a `planet` instance
    // as an argument...
    return function(planet) {
      var lastTick = null;
      var paused = false;
      planet.plugins.autorotate = {
        pause:  function() { paused = true;  },
        resume: function() { paused = false; }
      };
      // ...and configure hooks into certain pieces of its lifecycle.
      planet.onDraw(function() {
        if (paused || !lastTick) {
          lastTick = new Date();
        } else {
          var now = new Date();
          var delta = now - lastTick;
          // This plugin uses the built-in projection (provided by D3)
          // to rotate the globe each time we draw it.
          var rotation = planet.projection.rotate();
          rotation[0] += degPerSec * delta / 1000;
          if (rotation[0] >= 180) rotation[0] -= 360;
          planet.projection.rotate(rotation);
          lastTick = now;
        }
      });
    };
  };

  })();